# ---------------------------------------------------------------------------
# sistemas testado que estao funcionando corretamente
# ---------------------------------------------------------------------------

Rev.63 - Debugando...
Rev.62 - Criando mensagens de debug para a opcao: Config.DEVELOPER
Rev.61 - Fix na posição dos npcs de algumas quests
Rev.60 - Fix no sistema de target
Rev.59 - Adicionado verificação extra para previnir duplicação de itens
Rev.58 - Implementado recursos para evitar bugs no territory war
Rev.57 - Ajuste, nao retail, em algumas skills (Fear, cancel e Rushs)
Rev.56 - Pequena correção no GeoEngine
Rev.55 - Definindo item para ser consumido no Teleport Bookmark
Rev.54 - Mudando algumas frases da tela de carregamento do GameServer
Rev.53 - CreatureSee alterado para taskmanage
Rev.52 - Alterando GameTimeController para GameTimeTaskManager
Rev.51 - Adicionado algumas AI
Rev.50 - Movido arquivos referentes a castelos para pasta especifica
Rev.49 - Sistema de recipes reestruturado
Rev.48 - Desativado sistema customizado l2jmobius para GameServer e LoginServer
Rev.47 - Adicionado marcador para rotas diagonais no GeoEngine
Rev.46 - Fix no sistema de combat update
Rev.45 - Criado arquivos de Backup.psc e Backup.sql
Rev.44 - Alterado Tema do LoginServer e GameServer (Ficou Bonito)
Rev.44 - Fix no painel do GameServer e LoginServer
Rev.43 - Fix Skills
Rev.42 - Separado todas as IA por areas de territorios
Rev.41 - Movido todas as htmls das quests para a pasta html
Rev.40 - Alterado a estrutura das quests para um novo padrao
Rev.39 - Add custom skills de clan do God Of Destruction (Contribuicao KingHanker)
Rev.38 - Adicionado sets que faltavam
Rev.37 - Fix no problema ao iniciar as quests
Rev.36 - Fix Skills
Rev.35 - Adicionado IA para alguns Mobs de Antharas Lair
Rev.34 - Fix em pequeno erro na hora de entregar itens
Rev.33 - Fix Skills
Rev.32 - Fix Skills
Rev.31 - Fix Skills
Rev.30 - Fix Dragon Vortex
Rev.29 - Fix coordenadas de teleport do NPC Rooney(Forge Of Gods)
Rev.28 - Fix em algumas quests
Rev.27 - Fix Res de Admin
Rev.26 - Fix nas coordenadas dos teleports de Elroki
Rev.25 - Melhorando o codigo de Den of Evil
Rev.24 - Fix: Problema com o tempo de redefinição das instancias
Rev.23 - Quando o GM deleta uma missao, o jogador nao precisa mais fazer login novamente
Rev.22 - Fix Skills
Rev.21 - Ajuste no Lethal e Steal of Divinity
Rev.20 - Fix: Recompensas do Bau do Tesouro das Olimpiadas.
Rev.19 - Fix: Height de alguns NPCS, skills e local de spawn
Rev.18 - Fix: Problema com Holy Pomanders
Rev.17 - Fix: TriggerSkillByAttack precisa funcionar apenas para ataques normais
Rev.16 - Fix: Tipo de objeto errado passado para o formatador da siege
Rev.15 - Fix no sistema de delevel
Rev.14 - Ajustando as skills para não apresentar erros apos o ultimo update
Rev.13 - Criacao de novos Enums
Rev.12 - Suporte inicial para AffectScope.java
Rev.11 - Fix: Npcs tipo Walkers paravam quando tinham obstaculos
Rev.10 - Fix: range das skills dos npcs estava padrao para todos e nao como definido na skill
Rev.9 - Fix: Npcs da classe archers nao se moviam durante o ataque/atacado
Rev.8 - Fix: Multitarget (limite estava infinito)
Rev.7 - Mudando o projeto para o java 16
Rev.6 - Acertando o nome do projeto (mudando de l2jmobius para l2jhefesto)
Rev.5 - Ajuste de configs
Rev.4 - DataPack (Revisao base l2jMobius: https://l2jmobius.org)
Rev.3 - Java (Revisao base l2jMobius: https://l2jmobius.org)
Rev.2 - Iniciando o projeto (Up dos arquivos iniciais)
Rev.1 - Initial commit(criacao do projeto)
